using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manosjugador : MonoBehaviour
{
    public GameObject explosion;
    public GameObject reja;
    public int numerodeobjeto;
    public GameObject bolsillo; // referencio donde se va posicionar el objeto que agarre
    public GameObject bolsilloocupado = null; // el bolsillo del jugador inicia null osea vacio 
    public GameObject desactivarpostprocessing;


    void Update()
    {   
        if(bolsilloocupado != null) // si mi bolsillo contiene un objeto
        {
            if (Input.GetKey("r")) // si preciono la r va soltar el objeto
            {
                switch(numerodeobjeto)
                {
                    case 1: // objeto 1 corresponde a item mina explosiva
                        bolsilloocupado.GetComponent<Rigidbody>().useGravity = true; // activamos la gravedad del objeto
                        bolsilloocupado.GetComponent<Rigidbody>().isKinematic = false; // le desactivamos iskinematic
                        bolsilloocupado.gameObject.transform.SetParent(null); // desasociamos el objeto del jugador
                        bolsilloocupado = null; // indicamos que el bolsillo esta vacio para poder agarrar otras cosas nuevamente
                        numerodeobjeto = 0;                        
                        break;

                    case 2: // objeto 2 corresponde a item hierro
                        bolsilloocupado.GetComponent<Rigidbody>().useGravity = true; // activamos la gravedad del objeto
                        bolsilloocupado.GetComponent<Rigidbody>().isKinematic = false; // le desactivamos iskinematic
                        bolsilloocupado.GetComponent<BoxCollider>().isTrigger = false;
                        Vector3 escalaoriginal = new Vector3(0.1f, 0.1f, 1.5f);
                        bolsilloocupado.transform.localScale = escalaoriginal;
                        bolsilloocupado.gameObject.transform.SetParent(null); // desasociamos el objeto del jugador
                        bolsilloocupado = null; // indicamos que el bolsillo esta vacio para poder agarrar otras cosas nuevamente
                        
                        numerodeobjeto = 0;
                        break;
                }


            }
            if(numerodeobjeto == 2)
            {  
                bolsilloocupado.transform.position = bolsillo.transform.position;
                bolsilloocupado.GetComponent<Rigidbody>().position = bolsillo.GetComponent<Rigidbody>().position;
                bolsilloocupado.GetComponent<Rigidbody>().isKinematic = false;
                bolsilloocupado.transform.rotation = bolsillo.transform.rotation;
                bolsilloocupado.GetComponent<BoxCollider>().isTrigger = true;

                if (Input.GetKey("q")) {
                    Vector3 mover = new Vector3(0, 0, 1);
                    bolsilloocupado.transform.position = bolsilloocupado.transform.position + mover;
                    
                }
            }
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("itembomba"))
        {
            numerodeobjeto = 1;
            if (Input.GetKey("e") && bolsilloocupado == null)
            {
                other.GetComponent<Rigidbody>().useGravity = false; // desactivamos la gravedad del objeto que guardamos en el bolsillo
                other.GetComponent<Rigidbody>().isKinematic = true; // le activamos iskinematic para que no colisione con nada
                other.transform.position = bolsillo.transform.position; // llevamos el objeto a la posicion de nuestro bolsillo 
                other.gameObject.transform.SetParent(bolsillo.gameObject.transform); // hacemos que bolsillo sea padre del objeto recolectado
                bolsilloocupado = other.gameObject;

            }
        }

        if (other.gameObject.CompareTag("hierro")) // si recolecto objeto hierro
        {
            if (bolsilloocupado == null) // y el bolsillo esta vacio
            {
                if (Input.GetKey("e")) // y apreto la e
                {
                    Gestordeaudio.instancia.ReproducirSonido("agarrarhierro");
                    numerodeobjeto = 2;
                    other.GetComponent<Rigidbody>().useGravity = false; // desactivamos la gravedad del objeto que guardamos en el bolsillo
                    other.GetComponent<Rigidbody>().isKinematic = false; // necesitamos que si colisione                    
                    other.transform.position = bolsillo.transform.position; // llevamos el objeto a la posicion de nuestro bolsillo 
                    other.gameObject.transform.SetParent(bolsillo.gameObject.transform); // hacemos que bolsillo sea padre del objeto recolectado
                    //other.GetComponent<Rigidbody>().position = bolsillo.GetComponent<Rigidbody>().position;
                    bolsilloocupado = other.gameObject; // referenciamos que el bolsillo ahora se encuentra ocupado
                }
            }
        }

        

        if (other.gameObject.CompareTag("explosion"))
        {
            explosion.SetActive(false);
        }


    }
    }


