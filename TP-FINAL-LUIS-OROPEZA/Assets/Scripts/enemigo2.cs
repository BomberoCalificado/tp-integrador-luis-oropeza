using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigo2 : MonoBehaviour
{
   
    public Transform aquienmiro;
    public LayerMask aquienpersigo;  // EN QUE CAPA VA DETECTAR OBJETOS
    public float rangovision;
    public bool alerta;  // ESTA ALERTA O NO?
    public float Velocidadpersecución;
    public float vida = 50;

    // Start is called before the first frame update
    void Start()
    {
        alerta = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (vida <= 0)
        {
           
            gameObject.SetActive(false);
            
            
        }
        

        alerta = Physics.CheckSphere(transform.position, rangovision, aquienpersigo);  // VERIFICO SI JUGADOR ESTA EN EL RADIO DEL ENEMIGO
        if (alerta == true)  // SI EL ENEMIGO ESTA ALERTA ENTONCES..
        {
            Gestordeaudio.instancia.ReproducirSonido("enemigob");
            // CREAMOS ESTE VECTOR PARA GUARDAR POSICION DEL JUGADOR DETECTADO EN X,Z
            Vector3 Pj = new Vector3(aquienmiro.position.x, transform.position.y, aquienmiro.position.z);
            // DIRIGO LA MIRADA AL VECTOR DEL JUGADOR,X,Z, CONSERVANDO LA POSICION DEL ENEMIGO EN Y
            transform.LookAt(Pj);
            // TRANSFORMO LA POSICION DEL PUNTO A (POSICION DEL ENEMIGO) AL PUNTO B (VECTOR DEL JUGADOR XZ) A LA VELOCIDAD * TIME.DELTATIME
            transform.position = Vector3.MoveTowards(transform.position, Pj, Velocidadpersecución * Time.deltaTime);
        
        }

        

    }


    private void OnDrawGizmos()  //FUNCION PARA DIBUJAR LA VISION DEL ENEMIGO O EL RADIO, EN LA ESCENA
    {
        Gizmos.DrawWireSphere(transform.position, rangovision);
        if (alerta == true)
        {
            
        }
    }

   


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("hierro"))
        {
            if (Input.GetKey("q"))
            {
                Gestordeaudio.instancia.ReproducirSonido("golpearreja");
                vida = vida - 10;
                Vector3 mover = new Vector3(1, 0, 1);
                gameObject.transform.position = gameObject.transform.position + mover;
            }

        }
        if (other.gameObject.CompareTag("explosion"))
        {
           
            {
                Gestordeaudio.instancia.ReproducirSonido("gritomujer");
                vida = 0;
            }

        }
    }
}

