using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Controljugador : MonoBehaviour
{
    public GameObject desactivarprocessing;
    public GameObject cartelganaste;
    public GameObject cartelperdiste;
    public GameObject musicadeljuego;
    public GameObject objetojugador;
    public GameObject ubicacionjugador;
    public Text tiempo;
    public Text cantidadvida;
    public Text habilidad;
    public bool ganaste = false;
    public int vidajugador = 200;
    public int itembomba;
    public float rapidezDesplazamiento = 10.0f;
    public bool sigilo;
    public float tempsigilo;
    public float tiemporestante;

    void Start() 
    {
        sigilo = false;
        
        Cursor.lockState = CursorLockMode.Locked; }
    

    void Update()
    {
        tiemporestante -= Time.deltaTime;

        tiempo.text = "AUTODESTRUCCION DEL LABORATORIO EN  :  " + tiemporestante.ToString("f0");
        cantidadvida.text = "VIDA:  " + vidajugador.ToString();


        if (tiemporestante < 1)
        {
            vidajugador = 0;
                 }

        if (ganaste == true) {
            cartelganaste.SetActive(true);
            Time.timeScale = 0;
            if (Input.GetKey("m"))
            {

                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                Time.timeScale = 1;
            }
        } // SE EJECUTA SI GANASTE

        if (vidajugador <= 0)
        {
            vidajugador = 101;
            Gestordeaudio.instancia.ReproducirSonido("perdiste");
            musicadeljuego.SetActive(false);
        } // SE EJECUTA SI MORISTE

        if (sigilo == false && Input.GetKey("g")) // AL PRECIONAR G ENTRAS EN MODO INDETECTABLE
        {
           
            ubicacionjugador.gameObject.transform.SetParent(null);
            objetojugador.gameObject.layer = 0;
            rapidezDesplazamiento = 3;            
            sigilo = true;

        } // SIGILO

        if (sigilo == true)
        {
        tempsigilo -= Time.deltaTime;
        habilidad.text = "SIGILO ACTIVO: " + tempsigilo.ToString("F0");
        }

        if (tempsigilo <= 0) // SI PASAN LOS 20 SEG VUELVES A SER DETECTABLE
        {

            sigilo = false;

        }

        if (sigilo == false)
        {
            habilidad.text = "HABILIDAD: PRECIONAR (G)";
            ubicacionjugador.gameObject.transform.position = gameObject.transform.position;
            ubicacionjugador.gameObject.transform.SetParent(gameObject.transform);
            objetojugador.gameObject.layer = 6;
            rapidezDesplazamiento = 6;
        }

       



        if (vidajugador == 101)
        {
            
           
            cartelperdiste.SetActive(true);
            Time.timeScale = 0;

            if (Input.GetKey("m"))
            {

                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                Time.timeScale = 1;
            }
        } // REINICIAR ESCENA
            float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
            float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
            movimientoAdelanteAtras *= Time.deltaTime; movimientoCostados *= Time.deltaTime;
            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape")) 
        { Cursor.lockState = CursorLockMode.None; }
    }

    private void Sigilo()
    {
        if (sigilo == false && Input.GetKeyDown("c"))
        {

            gameObject.layer = 0;
            rapidezDesplazamiento = 3;
            // gameObject.layer = default;
            sigilo = true;

        } // AGACHARSE
        if (sigilo == true && Input.GetKeyDown("c"))
        {

            gameObject.layer = 6;
            rapidezDesplazamiento = 6;
            sigilo = false;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("arma2"))
        {

            {

                vidajugador = vidajugador - 30;
                Gestordeaudio.instancia.ReproducirSonido("gritomujer");
            }

        }

        if (other.gameObject.CompareTag("tagenemigo"))
        {
            
            {
                
                vidajugador = vidajugador - 10;
                
            }

        }
        if (other.gameObject.CompareTag("enemigogrande"))
        {

            {

                vidajugador = vidajugador - 50;

            }

        }
       
        if (other.gameObject.CompareTag("ganaste"))
        {

            ganaste = true;
            Gestordeaudio.instancia.ReproducirSonido("ganaste");
           
        }
        if (other.gameObject.CompareTag("pildora")) // si recolecto objeto pildora
        {
            if (Input.GetKey("e")) // y apreto la e
            {
                vidajugador = vidajugador + 50;
                Gestordeaudio.instancia.ReproducirSonido("tomarpildora");
                desactivarprocessing.SetActive(false);
            }
        }

    }
}