using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemhierro : MonoBehaviour
{
    //public GameObject hierro;
    public int vidadeobjeto = 12;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (vidadeobjeto == 0)
        {
            gameObject.SetActive(false);
            gameObject.transform.SetParent(null); // desasociamos el objeto del jugador
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("rejacelda1"))
        {
            if (Input.GetKey("q"))
            {
                Gestordeaudio.instancia.ReproducirSonido("golpearreja");
                vidadeobjeto--;
            }

        }
        if (other.gameObject.CompareTag("tagenemigo"))
        {
            if (Input.GetKey("q"))
            {
                Gestordeaudio.instancia.ReproducirSonido("golpearreja");
                vidadeobjeto--;
            }

        }
    }


}