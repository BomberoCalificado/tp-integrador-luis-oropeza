using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rejas : MonoBehaviour

{
    public GameObject destruirestareja;
    public int golpesdehierro;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (golpesdehierro == 5)
        {
            destruirestareja.SetActive(false);
            Gestordeaudio.instancia.ReproducirSonido("romper");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("hierro")) {
            if (Input.GetKey("q"))
            {
                Gestordeaudio.instancia.ReproducirSonido("golpearreja");
                golpesdehierro++;
            }
        }

    }
}
