using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botonpuertas : MonoBehaviour
{
    public GameObject abrirpuerta;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("tagjugador"))
        {
            // SI SE CUMPLE LA SENTENCIA ENTONCES DISPARAMOS EVENTO

            if (Input.GetKey("e"))
            {
                abrirpuerta.SetActive(false);
            }
            if (Input.GetKey("r"))
            {
                abrirpuerta.SetActive(true);
            }
        }
    }
}
