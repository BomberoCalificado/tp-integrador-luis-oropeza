using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paredespejada : MonoBehaviour
{
    public int colision;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    { 
        if(colision == 1)
        {
            gameObject.SetActive(false);
            
        }
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("explosion"))
        {
            colision = 1;
            
            Gestordeaudio.instancia.ReproducirSonido("romper");
        }
    }
}
