using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class minaexplosiva : MonoBehaviour
{
    public float temporizador = 0;
    public GameObject explosion;    
    public GameObject borrarbomba;    
    public LayerMask aquiendetecto; // en que capa detecta objetos para activar la explosion
    public bool estado = false;     // estado de deteccion de del rango de la bomba
    public float rango;             // rango de la bomba
    public int bombaactiva = 1; // si la bomba esta activa para detectar o no
    

    void Update()
    {
        if (bombaactiva == 1)
        {
            temporizador = 1 + Time.time;
            Gestordeaudio.instancia.ReproducirSonido("temporizador");
        }


            estado = Physics.CheckSphere(transform.position, rango, aquiendetecto);
            if (bombaactiva == 1 && estado == true)
            {
                explosion.SetActive(true);                
                explosion.gameObject.transform.SetParent(null);                
               
                borrarbomba.SetActive(false); 
                
                Gestordeaudio.instancia.PausarSonido("temporizador");
                Gestordeaudio.instancia.ReproducirSonido("explosion");
                Gestordeaudio.instancia.ReproducirSonido("gritomujer");                
                
            }

            if (temporizador > 20)
        {
               explosion.SetActive(true);              
               explosion.gameObject.transform.SetParent(null);
               borrarbomba.SetActive(false);
               Gestordeaudio.instancia.PausarSonido("temporizador");
               Gestordeaudio.instancia.ReproducirSonido("explosion");
        }

            
        
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("piso"))
        {
            if (Input.GetKey("f"))
            {
                bombaactiva = 1;
                Gestordeaudio.instancia.ReproducirSonido("temporizador");
            }
           
             
                      
        }
        
    }

    private void OnDrawGizmos()  //FUNCION PARA DIBUJAR LA VISION DEL ENEMIGO O EL RADIO, EN LA ESCENA
    {
        Gizmos.DrawWireSphere(transform.position,rango);
    }
}
